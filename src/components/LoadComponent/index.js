import React from 'react';
import { dynamic } from 'umi';
import Loading from '../Loading';

/**
 * 动态加载组件
 * 注：需要开umijs配置，dynamicImport: {}
 * @param {*} type 
 * @param {*} category 
 * @returns 
 */
const dynamicLoadingComponent = (type, category) => {
  return dynamic({
    loader: async function () {
      const { default: comp } = await import(`@/widgets/${category}/${type}`);
      const Component = comp;
      return (props) => {
        const { defaultValue } = props;
        return <Component {...defaultValue} />;
      };
    },
    loading: () => (
      <div style={{ paddingTop: 10, textAlign: 'center' }}>
        <Loading />
      </div>
    ),
  });
};

const LoadComponent = (props) => {
  const { type, category } = props;
  const Component = dynamicLoadingComponent(type, category);
  return <Component {...props} />;
};

export default LoadComponent;
