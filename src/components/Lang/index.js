import { Menu, Dropdown } from 'antd';
import { TranslationOutlined } from '@ant-design/icons';
import classnames from 'classnames';
import styles from './index.less';

const Lang = ({ className }) => {
  const menu = (
    <Menu>
      <Menu.Item key="0">
        <a>简体中文</a>
      </Menu.Item>
      <Menu.Item key="1">
        <a>Englist</a>
      </Menu.Item>
    </Menu>
  );
  return (
    <Dropdown className={classnames(styles.translate, className)} overlay={menu} trigger={['hover']}>
      <a onClick={e => e.preventDefault()}>
        <TranslationOutlined />
      </a>
    </Dropdown>
  );
};

export default Lang;