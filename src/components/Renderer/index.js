import GridLayout from 'react-grid-layout';
import classnames from 'classnames';
import LoadComponent from '../LoadComponent';
import styles from './index.less';

/**
 * H5页面渲染器
 * 根据node data渲染小部件
 * GridLayout 属性说明
 * cols={24} 栅格分为24列
 * rowHeight={1} 每一行的高度 1px，这个关系到小部件那边设置 h 的值
 * width={width} Grid的宽度
 * margin={[0, 0]} Grid Item直接的左右，上下间隔
 * @param {*} props 
 * @returns 
 */
const Renderer = props => {
  const {
    preview,
    nodeData,
    currNode,
    pageData,
    onDragStop,
    onDragStart,
    onResizeStop,
  } = props;
  const { bgColor, bgImage } = pageData || {};

  const getPageStyle = () => {
    return {
      // TODO: 需要通过属性设置画布的高度，现在是有自动变高
      //height: '864px',
      minHeight: '664px',
      backgroundColor: bgColor,
      backgroundImage: bgImage && bgImage.length > 0 ? `url(${bgImage[0].url})` : 'initial',
      backgroundSize: '100%',
      backgroundRepeat: 'no-repeat',
    };
  };

  return (
    <GridLayout
      cols={24}
      rowHeight={1}
      width={375}
      margin={[0, 0]}
      isResizable={true}
      onDragStop={onDragStop}
      onDragStart={onDragStart}
      onResizeStop={onResizeStop}
      style={getPageStyle()}
    >
      {
        nodeData && nodeData.map(value => {
          const gridItemProps = preview ? {
            ...value.point,
            isDraggable: false,
            isResizable: false,
            isBounded: false,
          } : value.point;
          return (
            <div
              key={value.id}
              data-grid={gridItemProps}
              className={classnames({
                [`${styles.item}`]: !preview,
                [`${styles.active}`]: currNode && currNode.id === value.id,
              })}
            >
              <LoadComponent {...value.item} />
            </div>
          );
        })
      }
    </GridLayout>
  );
};

export default Renderer;
