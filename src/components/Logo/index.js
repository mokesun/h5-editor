import { Button } from 'antd';
import classnames from 'classnames';
import styles from './index.less';

const Logo = ({ className }) => {
  return (
    <div className={classnames(styles.logo, className)}>
      Logo
    </div>
  );
};

export default Logo;