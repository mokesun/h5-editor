import { isObject } from 'lodash';

function uuid(len, radix) {
  let chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
  let uuid = [],
    i;
  radix = radix || chars.length;

  if (len) {
    for (i = 0; i < len; i++) uuid[i] = chars[0 | (Math.random() * radix)];
  } else {
    let r;
    uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
    uuid[14] = '4';

    for (i = 0; i < 36; i++) {
      if (!uuid[i]) {
        r = 0 | (Math.random() * 16);
        uuid[i] = chars[i === 19 ? (r & 0x3) | 0x8 : r];
      }
    }
  }

  return uuid.join('');
}

function decodeURIData(data) {
  if (!data) {
    return null;
  }
  try {
    const temp = decodeURIComponent(data);
    return JSON.parse(temp);
  } catch {
    return null;
  }
}

function encodeURIData(data) {
  if (!data) {
    return '';
  }
  if (!isObject(data)) {
    return data;
  }
  try {
    const temp = JSON.stringify(data);
    return encodeURIComponent(temp);
  } catch {
    return '';
  }
}

export {
  uuid,
  encodeURIData,
  decodeURIData,
}