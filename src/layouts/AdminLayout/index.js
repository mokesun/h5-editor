import { useState } from 'react';
import { Layout, Button } from 'antd';
import { MenuUnfoldOutlined, MenuFoldOutlined } from '@ant-design/icons';
import { Logo, Lang } from '@/components';
import { Nav, Links, User } from './components';
import styles from './index.less';

const { Header, Content, Sider } = Layout;

const AdminLayout = ({ children }) => {
  const [collapsed, setCollapsed] = useState(false);
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sider theme="light" trigger={null} collapsible collapsed={collapsed}>
        <Logo className={styles.logo} />
        <Nav />
      </Sider>
      <Layout>
        <Header className={styles.header}>
          <Button
            className={styles.collapsed}
            type="link"
            onClick={() => setCollapsed(!collapsed)}
          >
            {
              collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />
            }
          </Button>
          <Links className={styles.links} />
          <Lang className={styles.lang} />
          <User className={styles.profile} />
        </Header>
        <Content className={styles.content}>
          {children}
        </Content>
      </Layout>
    </Layout>
  );
};

export default AdminLayout;
