import { Button } from 'antd';
import { history } from 'umi';
import classnames from 'classnames';
import styles from './index.less';

const Links = ({ className }) => {
  const handleNewH5 = () => {
    history.push({
      pathname: '/editor',
    });
  };
  return (
    <div className={classnames(styles.links, className)}>
      <Button type="link" onClick={handleNewH5}>New H5</Button>
    </div>
  );
};

export default Links;