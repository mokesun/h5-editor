import React from 'react';
import { Menu } from 'antd';
import { UserOutlined } from '@ant-design/icons';

const { SubMenu } = Menu;

const Nav = () => (
  <Menu mode="inline" defaultSelectedKeys={['1']}>
    <Menu.Item key="1" icon={<UserOutlined />}>
      H5
    </Menu.Item>
  </Menu>
);

export default Nav;