import { Menu, Avatar, Dropdown } from 'antd';
import { UserOutlined, LogoutOutlined, SettingOutlined } from '@ant-design/icons';
import classnames from 'classnames';
import styles from './index.less';

const User = ({ className }) => {
  const menu = (
    <Menu>
      <Menu.Item key="0">
        <a><UserOutlined /> Profile</a>
      </Menu.Item>
      <Menu.Item key="1">
        <a><SettingOutlined /> Change Password</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="3"><LogoutOutlined /> Logout</Menu.Item>
    </Menu>
  );
  return (
    <Dropdown className={classnames(styles.profile, className)} overlay={menu} trigger={['hover']}>
      <a onClick={e => e.preventDefault()}>
        <Avatar src="https://joeschmoe.io/api/v1/random" style={{ width: 32 }} />
        <span>Admin</span>
      </a>
    </Dropdown>
  );
};

export default User;