import { useState } from 'react';
import { history } from 'umi';
import { Layout, Button } from 'antd';
import { ArrowLeftOutlined } from '@ant-design/icons';
import { Logo, Lang } from '@/components';
import { Links } from './components';
import styles from './index.less';

const { Header, Content } = Layout;

const EditorLayout = ({ children }) => {
  const handleBack = () => {
    // TODO: 先检查是否已经保存
    // Do back
    history.goBack();
  }
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Header className={styles.header}>
        <Button type="link" className={styles.back} onClick={handleBack}><ArrowLeftOutlined /></Button>
        <Logo className={styles.logo} />
        <Links className={styles.links} />
        <div className={styles.lang}><Lang /></div>
      </Header>
      <Content className={styles.content}>
        {children}
      </Content>
    </Layout>
  );
};

export default EditorLayout;
