import { connect } from 'umi';
import { Button } from 'antd';
import { encodeURIData } from '@/utils';
import { CopyOutlined, SnippetsOutlined, RedoOutlined, UndoOutlined, DeleteOutlined } from '@ant-design/icons';
import classnames from 'classnames';
import styles from './index.less';

const Links = ({ className, nodeData }) => {
  const handlePreview = () => {
    window.open(`/preview?pageData=&nodeData=${encodeURIData(nodeData)}`);
  };
  return (
    <div className={classnames(styles.links, className)}>
      <Button type="link">模板库</Button>
      <Button type="link">保存</Button>
      <Button type="link" onClick={handlePreview}>预览</Button>
      <Button type="link">导入</Button>
      <Button type="link">导出</Button>
      <Button type="link"><CopyOutlined /></Button>
      <Button type="link"><SnippetsOutlined /></Button>
      <Button type="link"><RedoOutlined /></Button>
      <Button type="link"><UndoOutlined /></Button>
      <Button type="link"><DeleteOutlined /></Button>
    </div>
  );
};

export default connect(({ editor }) => ({
  nodeData: editor.nodeData,
}))(Links);