import Browser from './Browser';
import PageInfo from './PageInfo';

export {
  Browser,
  PageInfo,
};