
import { history } from 'umi';
import { get } from 'lodash';
import { decodeURIData } from '@/utils';
import { Renderer } from '@/components';
import styles from './index.less';

const Browser = () => {
  const nodeData = decodeURIData(get(history, 'location.query.nodeData')) || [];
  const pageData = decodeURIData(get(history, 'location.query.pageData'));
  console.log(nodeData);
  return (
    <div className={styles.container}>
      <div className={styles.view}>
        <Renderer
          preview={true}
          nodeData={nodeData}
          pageData={pageData}
        />
      </div>
    </div>
  );
};

export default Browser;