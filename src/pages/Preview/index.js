
import { Row, Col } from 'antd';
import { Browser, PageInfo } from './components';
import styles from './index.less';

/**
 * 预览界面
 * 这里只是简单的预览，正常的预览逻辑应该是：
 * 1、先保存草稿
 * 2、传H5页面ID到预览界面
 * 3、从接口获取H5页面的nodeData，pageNode
 * 4、开始初始化界面
 * @returns 
 */
const Preview = () => {
  return (
    <div className={styles.container}>
      <Row gutter={16}>
        <Col span={14}>
          <Browser />
        </Col>
        <Col span={10}>
          <PageInfo />
        </Col>
      </Row>
    </div>
  );
};

export default Preview;