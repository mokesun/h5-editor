import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { Widgets, Attributes, WorkSpace } from './components';
import styles from './index.less';

/**
 * 编辑器容器页面
 * 使用DndProvider包裹，提供拖拽能力，使用细节见 react-dnd
 * @returns 
 */
const Editor = () => {
  return (
    <DndProvider backend={HTML5Backend}>
      <div className={styles.container}>
        <Widgets className={styles.widgetPannel} />
        <WorkSpace className={styles.workPannel} />
        <Attributes className={styles.attrPannel} />
      </div>
    </DndProvider>
  );
}

export default Editor;
