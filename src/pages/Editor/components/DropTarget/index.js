import { useDrop } from 'react-dnd';
import { uuid } from '@/utils';
import schema from '@/widgets/schema';
import styles from './index.less';

/**
 * 拖放目标组件
 * 主要接收拖拽源的数据，提交到state
 * @param {*} props 
 * @returns 
 */
const DrogTarget = props => {
  const { children, parentId, dispatch } = props;

  const types = Object.values(schema).reduce((prev, next) => {
    const types = next.map(n => n.type);
    return [...prev, ...types];
  }, []);

  const [{ isOver }, drop] = useDrop({
    accept: types,
    drop: (item, monitor) => {
      let parentDiv = document.getElementById(parentId);
      let pointRect = parentDiv.getBoundingClientRect();
      let top = pointRect.top;
      let pointEnd = monitor.getSourceClientOffset();
      let y = pointEnd.y < top ? 0 : pointEnd.y - top;
      let col = 24; // 网格列数
      let cellHeight = 1;
      let w = col;
      // 转换成网格规则的坐标和大小
      let gridY = Math.ceil(y / cellHeight);
      let id = uuid(6, 10);
      dispatch({
        type: 'editor/addNodeData',
        payload: {
          id: id,
          item,
          point: {
            w,
            i: `x-${id}`,
            x: 0,
            y: gridY,
            h: item.h,
            isDraggable: true,
            isResizable: true,
            isBounded: true,
          },
        },
      });
    },
    collect: monitor => ({
      isOver: monitor.isOver(),
      item: monitor.getItem(),
    }),
  });

  return (
    <div ref={drop} className={styles.container} style={{ opacity: isOver ? 0.4 : 1 }}>
      {children}
    </div>
  )
};

export default DrogTarget;
