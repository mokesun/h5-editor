import { useState } from 'react';
import { connect } from 'umi';
import { Button, Result, Divider } from 'antd';
import { DoubleLeftOutlined, DoubleRightOutlined } from '@ant-design/icons';
import classnames from 'classnames';
import FormRender from '../FormRender';
import styles from './index.less';

/**
 * 小部件属性编辑面板
 * 当前节点currNode如果存在，调用动态表单FormRender渲染，并监听表单的保存事件
 * 保存表单，改变state，设计画布重新渲染最新的效果
 * @param {*} props 
 * @returns 
 */
const Attributes = props => {
  const { className, currNode, dispatch } = props;

  const [collapsed, setCollapsed] = useState(false);

  const handleFormSave = data => {
    dispatch({
      type: 'editor/modNodeData',
      payload: { ...currNode, item: { ...currNode.item, defaultValue: data } },
    });
  };

  const classes = classnames(styles.container, className, {
    [`${styles.containerCollapsed}`]: collapsed,
  });

  return (
    <div className={classes}>
      <h3>属性设置</h3>
      <Divider />
      {
        currNode ? (
          <FormRender
            attributes={currNode.item.attributes}
            uid={currNode.id}
            defaultValue={currNode.item.defaultValue}
            onSave={handleFormSave}
          />
        ) : (
          <div style={{ paddingTop: '100px' }}>
            <Result status="404" title="还没有数据哦" subTitle="请选择一个组件来编辑吧～" />
          </div>
        )
      }
      <div className={styles.collapsed}>
        <Button type="link" onClick={() => setCollapsed(!collapsed)}>
          {
            collapsed ? <DoubleLeftOutlined /> : <DoubleRightOutlined />
          }
        </Button>
      </div>
    </div>
  );
}

export default connect(({ editor }) => ({
  currNode: editor.currNode,
}))(Attributes);
