import { useState } from 'react';
import { Tabs, Button } from 'antd';
import { AppstoreOutlined, PieChartOutlined, PlayCircleOutlined, DoubleLeftOutlined, DoubleRightOutlined } from '@ant-design/icons';
import classnames from 'classnames';
import schema from '@/widgets/schema';
import Item from './item';
import styles from './index.less';

const { TabPane } = Tabs;

/**
 * 组件库面板
 * 加载所有类别的小部件
 * @param {*} props 
 * @returns 
 */
const Widgets = props => {
  const { className } = props;
  const [collapsed, setCollapsed] = useState(false);

  const handleTabChange = () => {
    if (collapsed) {
      setCollapsed(false);
    }
  };

  const renderList = schemas => {
    return (
      <div className={styles.list}>
        {
          schemas.map(schema => (<Item key={schema.type} schema={schema} />))
        }
      </div>
    )
  };

  const classes = classnames(styles.container, className, {
    [`${styles.containerCollapsed}`]: collapsed,
  });

  return (
    <div className={classes}>
      <Tabs tabPosition="left" onChange={handleTabChange}>
        <TabPane tab={<AppstoreOutlined />} key="1">
          <h3>基础</h3>
          {renderList(schema.base)}
        </TabPane>
        <TabPane tab={<PieChartOutlined />} key="2">
          图表
        </TabPane>
        <TabPane tab={<PlayCircleOutlined />} key="3">
          多媒体
        </TabPane>
      </Tabs>
      <div className={styles.collapsed}>
        <Button type="link" onClick={() => setCollapsed(!collapsed)}>
          {
            collapsed ? <DoubleRightOutlined /> : <DoubleLeftOutlined />
          }
        </Button>
      </div>
    </div>
  );
}

export default Widgets;
