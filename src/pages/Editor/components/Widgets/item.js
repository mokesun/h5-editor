
import classnames from 'classnames';
import DragSource from "../DragSource";
import styles from './index.less';

const Item = props => {
  const { className, schema } = props;
  const { display: { thum, name } } = schema;
  return (
    <DragSource schema={schema}>
      <div className={classnames(styles.item, className)}>
        <div className={styles.thum}><img src={thum} alt={name}></img></div>
        <div className={styles.name}>{name}</div>
      </div>
    </DragSource>
  );
}

export default Item;
