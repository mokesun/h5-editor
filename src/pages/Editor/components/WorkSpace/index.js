import { connect } from 'umi';
import Draggable from 'react-draggable';
import classnames from 'classnames';
import Designer from '../Designer';
import Calibration from '../Calibration';
import Controls from '../Controls';
import ContextMenu from '../ContextMenu';
import styles from './index.less';

/**
 * 工作区域容器组件
 * 包括，刻度尺、缩放控制控件、还有最重要的设计画布
 * 注：设计画布是可以拖动的，主要实现Draggable
 * @param {*} props 
 * @returns 
 */
const WorkSpace = props => {
  const { className, scale } = props;
  return (
    <ContextMenu className={classnames(styles.container, className)}>
      <Calibration className={styles.topCalibration} direction="up" multiple={scale} />
      <Calibration className={styles.leftCalibration} direction="right" multiple={scale} />
      <Draggable handle="#draggableHandler">
        <div className={styles.draggableWrapper}>
          {/** TODO: 可拖动位置需要优化 */}
          <div id="draggableHandler" className={styles.draggableHandler}></div>
          <Designer
            className={styles.draggableContent}
            style={{ transform: `scale(${scale})` }}
          />
        </div>
      </Draggable>
      <Controls className={styles.controls} />
    </ContextMenu>
  );
};

export default connect(({ editor }) => ({
  scale: editor.scale,
}))(WorkSpace);