import { connect } from 'umi';
import classnames from 'classnames';
import styles from './index.less';

/**
 * 设计画布控制小控件，目前功能支持：缩放
 * @param {*} props 
 * @returns 
 */
const Controls = props => {
  const { className, scale, dispatch } = props;
  const handlePlus = () => {
    if (Math.floor(scale * 10) * 10 >= 150) {
      return;
    }
    dispatch({
      type: 'editor/setScale',
      payload: Number((scale + 0.1).toFixed(1)),
    });
  };
  const handleSub = () => {
    if (Math.floor(scale * 10) * 10 <= 50) {
      return;
    }
    dispatch({
      type: 'editor/setScale',
      payload: Number((scale - 0.1).toFixed(1)),
    });
  };
  return (
    <div className={classnames(styles.container, className)}>
      <div className={styles.plus} onClick={handlePlus}>+</div>
      <div className={styles.value}>{Math.floor(scale * 10) * 10}%</div>
      <div className={styles.sub} onClick={handleSub}>-</div>
    </div>
  );
};

export default connect(({ editor }) => ({
  scale: editor.scale,
}))(Controls);