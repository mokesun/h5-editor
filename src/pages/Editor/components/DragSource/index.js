import { useDrag } from 'react-dnd';
import styles from './index.less';

/**
 * 拖拽源组件
 * 任何被这个组件包裹的孩子组件都具备拖拽能力
 * @param {*} props 
 * @returns 
 */
const DragSource = props => {
  const { children, schema } = props;
  const { category, type, x, h, attributes, defaultValue } = schema;
  const [{ isDragging }, drag] = useDrag({
    type,
    item: {
      type,
      h,
      x,
      defaultValue: defaultValue,
      attributes: attributes,
      category: category,
    },
    collect: monitor => ({
      isDragging: monitor.isDragging(),
    }),
  });

  return (
    <div ref={drag} className={styles.container} style={{ opacity: isDragging ? 0.4 : 1 }}>
      {children}
    </div>
  )
};

export default DragSource;
