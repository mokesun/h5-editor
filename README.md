# H5 Editor

此项目是属于学习项目，并不是一个完整的产品。主要是学习开源项目:  <https://github.com/MrXujiang/h5-Dooring> 
在此基础上做了一些修改。


## Getting Started

Install dependencies,

```bash
$ yarn
```

Start the dev server,

```bash
$ yarn start
```


