import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [
    {
      path: '/',
      redirect: '/admin',
    },
    {
      path: '/admin',
      component: '@/layouts/AdminLayout',
      routes: [
        {
          title: 'Admin',
          path: '/admin',
          component: '@/pages/Home',
        }
      ]
    },
    {
      path: '/editor',
      component: '@/layouts/EditorLayout',
      routes: [
        {
          title: 'H5 Editor',
          path: '/editor',
          component: '@/pages/Editor',
        }
      ]
    },
    {
      path: '/preview',
      component: '@/pages/Preview',
    },
  ],
  dynamicImport: {},
  fastRefresh: {},
});
